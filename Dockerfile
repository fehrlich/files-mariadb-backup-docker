FROM alpine:latest
WORKDIR /usr/local/bin/

ENV FILE_PATH="/var/files"
ENV REMOTE_HOST="root@backupserver"
ENV REMOTE_PATH="/var/backup"
ENV BACKUP_CACHE="/var/cache/backup"

ENV DB_HOST="localhost"
ENV DB_USER="root"
ENV DB_PASSWORD=""
ENV DB_DB="myDatabase"
ENV BACKUP_KEEP=7
ENV COMMAND="backup"
ENV OBJECT="files"
ENV BACKUPFOLDER="current"
ENV BACKUPFOLDER_DEST="$(date +%Y%m%d%H%M%S)"
ENV CLEANUP_PATTERN="[0-9\-]{14}"
ENV RCLONE_CONFIG="/root/.config/rclone/rclone.conf"
ENV REMOTE_HOST_RCLONE=""

VOLUME [ "/root/.ssh" ]
VOLUME [ "/var/files" ]

RUN apk add \
    rsync \
    mariadb-client \
    openssh-client \
    curl \
    gzip && \
    curl -O https://downloads.rclone.org/rclone-current-linux-amd64.zip && \
    unzip rclone-current-linux-amd64.zip && \
    cd rclone-*-linux-amd64 && \
    cp rclone /usr/bin/ && \
    chown root:root /usr/bin/rclone && \
    chmod 755 /usr/bin/rclone && \
    apk add openssh && \
    ln -s /root/.ssh/id_rsa /etc/ssh/ssh_host_rsa_key

COPY commands /usr/local/bin/

RUN mkdir $BACKUP_CACHE && \
    chmod +x /usr/local/bin/*

ENTRYPOINT command.sh
