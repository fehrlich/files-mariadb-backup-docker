set -e
# cleanup
echo "Cleanup and keep $BACKUP_KEEP backups from the form of ${CLEANUP_PATTERN}\n"

BACKUP_KEEP_C=$(($BACKUP_KEEP - 1))
existing_raw="$(ssh ${REMOTE_HOST} "ls ${REMOTE_PATH}")"
[ "$?" -ne 0 ] && ssh ${REMOTE_HOST} "mkdir -p ${REMOTE_PATH}"
existing=$(echo "${existing_raw}" | egrep ${CLEANUP_PATTERN} | sort -nr)
index=0
for BACKUPNAME in ${existing}; do
    index=$(($index + 1))
    if [ "$index" -gt "$BACKUP_KEEP_C" ]; then
        echo "delete $BACKUPNAME"
        ssh ${REMOTE_HOST} rm -rf ${REMOTE_PATH}/$BACKUPNAME
    fi
done
