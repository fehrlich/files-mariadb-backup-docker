set -e
max=370
dayinseconds=60*60*24

for i in `seq 0 $max`; do
    diff=`expr -$dayinseconds*$max+$dayinseconds*$i`
    EXT_DATE=$(date -d@"$(( `date +%s`+$diff))" +%Y-%m-%d)
    day=${EXT_DATE: -2}
    month_day=${EXT_DATE: -5}
    isWeek=`expr $i % 7 == 0`
    isMonth=`expr $day == '01'`
    isYear=`expr $month_day == '01-01'`
    year=${EXT_DATE: 0:4}
    echo $EXT_DATE

    export OVERWRITE_DATE=$EXT_DATE
    ./example-daily.sh    

    if [ $isWeek -eq 1 ]; then
        export OVERWRITE_DATE="$EXT_DATE"
        ./example-weekly.sh
    fi
    if [ $isMonth -eq 1 ]; then
        export OVERWRITE_DATE=${EXT_DATE: 0:7}
        ./example-monthly.sh
    fi
    if [ $isYear -eq 1 ]; then
        export OVERWRITE_DATE="$year"
        ./example-yearly.sh
    fi
done