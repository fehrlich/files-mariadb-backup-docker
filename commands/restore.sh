set -e

for obj in $OBJECT
do
    if [ "$BACKUPFOLDER" ]; then
        export BACKUPFOLDER_WITHOBJECT="${BACKUPFOLDER}/${obj}"
    else
        export BACKUPFOLDER_WITHOBJECT="${obj}"
    fi

    if [ ! "$BACKUP_FILE" ]; then
        export BACKUP_FILE="latest_$obj.tar.gz"
    fi

    if [ "$BACKUP_FOLDER" ]; then
        export BACKUP_PATH="$BACKUP_FOLDER/$BACKUP_FILE"
    else
        export BACKUP_PATH="$BACKUP_FILE"
    fi

    export LOCAL_BACKUP=$BACKUP_CACHE/$BACKUPFOLDER/$obj
    export REMOTE_HOSTPATH=$REMOTE_HOST:$REMOTE_PATH


    if [ -f "./restore-$obj.sh" ]; then
        if [ "$USE_RCLONE" ]; then
            echo "receive backup: rclone copy $REMOTE_HOSTPATH_RCLONE/$BACKUP_PATH $BACKUP_CACHE/"
            $RCLONE_COMMAND copy $REMOTE_HOSTPATH_RCLONE/$BACKUP_PATH $BACKUP_CACHE/
        else
            echo "receive backup: $RSYNC_COMMAND_ZIP $REMOTE_HOSTPATH/$BACKUP_PATH $BACKUP_CACHE/"
            $RSYNC_COMMAND_ZIP $REMOTE_HOSTPATH/$BACKUP_PATH $BACKUP_CACHE/
        fi
        cd $BACKUP_CACHE
        echo "UNZIP: tar -xvzf $BACKUP_CACHE/$BACKUP_FILE"
        tar -xvzf $BACKUP_CACHE/$BACKUP_FILE 2>&1 >/dev/null
        cd /usr/local/bin
        ./restore-$obj.sh
    else
        echo "the object $obj is not supported for restore"
        exit 1
    fi
done
