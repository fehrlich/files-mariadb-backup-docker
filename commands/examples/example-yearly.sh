export CUR_DATE=$(date +%Y)

if [ ! -z "$OVERWRITE_DATE" ]; then
    CUR_DATE="$OVERWRITE_DATE"
fi

#move files backup to %Y%m%d-FILES and only keep 10 backups
export COMMAND=move
export OBJECT=files
export BACKUPFOLDER_DEST="$CUR_DATE-Y-FILES"
export CLEANUP_PATTERN="[0-9]{4}-Y-FILES"
export BACKUP_KEEP=10
./command.sh

#move files backup to %Y%m%d-FILES and only keep 10 backups
export COMMAND=move
export OBJECT=db
export BACKUPFOLDER_DEST="$CUR_DATE-Y-DB"
export CLEANUP_PATTERN="[0-9]{4}-Y-DB"
export BACKUP_KEEP=10
./command.sh