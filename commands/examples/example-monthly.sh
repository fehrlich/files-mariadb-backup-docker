export CUR_DATE=$(date +%Y-%m)

if [ ! -z "$OVERWRITE_DATE" ]; then
    CUR_DATE="$OVERWRITE_DATE"
fi

#move files backup to %Y%m%d-FILES and only keep 10 backups
export COMMAND=move
export OBJECT=files
export BACKUPFOLDER_DEST="$CUR_DATE-M-FILES"
export CLEANUP_PATTERN="[0-9\-]{7}-M-FILES"
export BACKUP_KEEP=10
./command.sh

#move files backup to %Y%m%d-FILES and only keep 10 backups
export COMMAND=move
export OBJECT=db
export BACKUPFOLDER_DEST="$CUR_DATE-M-DB"
export CLEANUP_PATTERN="[0-9\-]{7}-M-DB"
export BACKUP_KEEP=10
./command.sh