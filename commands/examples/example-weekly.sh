export CUR_DATE=$(date +%Y-%m-%d)

if [ ! -z "$OVERWRITE_DATE" ]; then
    CUR_DATE="$OVERWRITE_DATE"
fi

#move files backup to %Y%m%d-FILES and only keep 10 backups
export COMMAND=move
export OBJECT=files
export BACKUPFOLDER_DEST="$CUR_DATE-W-FILES"
export CLEANUP_PATTERN="[0-9\-]{10}-W-FILES"
export BACKUP_KEEP=10
./command.sh

#move files backup to %Y%m%d-FILES and only keep 10 backups
export COMMAND=move
export OBJECT=db
export BACKUPFOLDER_DEST="$CUR_DATE-W-DB"
export CLEANUP_PATTERN="[0-9\-]{10}-W-DB"
export BACKUP_KEEP=10
./command.sh