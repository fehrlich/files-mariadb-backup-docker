
export CUR_DATE=$(date +%Y-%m-%d)

if [ ! -z "$OVERWRITE_DATE" ]; then
    CUR_DATE="$OVERWRITE_DATE"
fi

#backup files
export COMMAND=backup
export OBJECT=files
./command.sh

#backup database
export COMMAND=backup
export OBJECT=db
./command.sh
#move database backup to %Y%m%d-DB and only keep 10 backups
export COMMAND=move
export OBJECT=db
export BACKUPFOLDER_DEST="$CUR_DATE-DB"
export CLEANUP_PATTERN="[0-9\-]{10}-DB"
export BACKUP_KEEP=10
./command.sh