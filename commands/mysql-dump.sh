set -e
if [ "$DEBUG" ]; then
	echo "INFO (exec): mysqldump -u${DB_USER} -p${DB_PASSWORD} -h${DB_HOST} ${DB_DB} -r \
		$BACKUP_CACHE/database.sql"
fi

mysqldump -u${DB_USER} -p${DB_PASSWORD} -h${DB_HOST} ${DB_DB} -r \
$BACKUP_CACHE/database.sql
