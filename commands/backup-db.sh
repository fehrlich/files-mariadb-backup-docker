set -e
#create mysql dump
echo "create database backup\n"
./mysql-dump.sh

# copy mysql dump
echo "copy database backup to remote\n"
$RSYNC_COMMAND $BACKUP_CACHE/database.sql $REMOTE_HOSTPATH/$BACKUPFOLDER_WITHOBJECT/database.sql

echo "cleanup database backup on local\n"
rm $BACKUP_CACHE/database.sql
