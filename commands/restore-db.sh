set -e

echo "execute sql: mysql -u${DB_USER} -p${DB_PASSWORD} -h${DB_HOST} ${DB_DB} < $LOCAL_BACKUP/database.sql"

mysql -u${DB_USER} -p${DB_PASSWORD} -h${DB_HOST} ${DB_DB} < $LOCAL_BACKUP/database.sql
