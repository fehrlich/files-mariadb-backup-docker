set -e
for obj in $OBJECT
do
    if [ "$BACKUPFOLDER" ]; then
        export BACKUPFOLDER_WITHOBJECT="${BACKUPFOLDER}/${obj}"
    else
        export BACKUPFOLDER_WITHOBJECT="${obj}"
    fi
    echo "ssh ${REMOTE_HOST} tar -C ${REMOTE_PATH} -zcf ${REMOTE_PATH}/$BACKUPFOLDER_DEST.tar.gz $BACKUPFOLDER_WITHOBJECT"
    ssh ${REMOTE_HOST} tar -C ${REMOTE_PATH} -zcf ${REMOTE_PATH}/$BACKUPFOLDER_DEST.tar.gz $BACKUPFOLDER_WITHOBJECT
    # z      (De)compress using gzip
    # c      Create
    # v      verbose
    # f      FILE Name of TARFILE ('-' for stdin/out)
    echo "Link: ${REMOTE_PATH}/current_$obj -> ${REMOTE_PATH}/$BACKUPFOLDER_DEST.tar.gz\n"
    ssh ${REMOTE_HOST} ln -f ${REMOTE_PATH}/$BACKUPFOLDER_DEST.tar.gz ${REMOTE_PATH}/latest_$obj.tar.gz
done
