set -e
if [ "$USE_RCLONE" ]; then
    echo $RCLONE_COMMAND copy $LOCAL_BACKUP/* $FILE_PATH/
    $RCLONE_COMMAND copy $LOCAL_BACKUP/* $FILE_PATH/
else
    cp -rf $LOCAL_BACKUP/* $FILE_PATH
fi
