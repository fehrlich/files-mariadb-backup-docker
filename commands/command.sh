set -e

export RSYNC_COMMAND="rsync -azhp --compress-level=9"
export RSYNC_COMMAND_ZIP="rsync -azhp"
export RCLONE_COMMAND="rclone --config $RCLONE_CONFIG"

if [ "$DEBUG" ]; then
    export RSYNC_COMMAND="${RSYNC_COMMAND} --progress"
fi

if [ "$REMOTE_USER" ]; then
    export REMOTE_HOST="$REMOTE_USER@$REMOTE_HOST"
fi

export REMOTE_HOSTPATH=$REMOTE_HOST:$REMOTE_PATH
export REMOTE_HOSTPATH_RCLONE=$REMOTE_HOST_RCLONE:$REMOTE_PATH

ex=0

if [ $COMMAND == "backup" ]; then
    ex=1
    ./backup.sh
fi
if [ $COMMAND == "restore" ]; then
    ex=1
    ./restore.sh
fi
if [ $COMMAND == "move" ]; then
    ex=1
    ./move.sh
fi

if [ exit == 0 ]; then
    echo "Command in env variable \$COMMAND not found use: backup, move or restore"
    exit 1;
fi

echo "Finished"
