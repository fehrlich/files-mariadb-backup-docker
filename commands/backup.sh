set -e
for obj in $OBJECT
do
    export CUR_OBJ=$obj
    if [ "$BACKUPFOLDER" ]; then
        export BACKUPFOLDER_WITHOBJECT="${BACKUPFOLDER}/${obj}"
    else
        export BACKUPFOLDER_WITHOBJECT="${obj}"
    fi
    if [ -f "./backup-$obj.sh" ]; then

        if [ "$USE_RCLONE" ]; then
            $RCLONE_COMMAND mkdir $REMOTE_HOSTPATH_RCLONE/$BACKUPFOLDER_WITHOBJECT/
        else
            ssh ${REMOTE_HOST} "mkdir -p $REMOTE_PATH/$BACKUPFOLDER_WITHOBJECT"
        fi
        ./backup-$obj.sh
    else
        echo "the object $obj is not supported for backup"
        exit 1
    fi
done
