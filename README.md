# Backup files

Files are copied one way, so they will be kept on deletion.

# Examples

For the correct usage of the ENV variables see the examples in the commands/example-* files.

# Testing